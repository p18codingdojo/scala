package myHelloWorldPackage

object Minesweeper extends App {
  var columns: Int = 0
  var rows: Int = 0
  lazy val BOMB = '*'

  lazy val EMPTY = '.'

  lazy val DELIMITER = "\n"

  def countMines(board: String, currentRow: Int, currentCol: Int): Int = {
    var mineCounter = 0
    for (neighbourRow <- currentRow - 1 to currentRow + 1) {
      for (neighbourCol <- currentCol - 1 to currentCol + 1) {
        if (neighbourCol >= 0 && neighbourCol < this.columns && neighbourRow >= 0 && neighbourRow < this.rows) {
          val index = neighbourCol + neighbourRow * this.columns
          if (board.charAt(index).equals(BOMB))
            mineCounter += 1
        }
      }
    }
    mineCounter
  }

  def getField(board: String, index: Int, i: Int, j: Int): String = {
    val currentChar = board.charAt(index)
    if (EMPTY.equals(currentChar)) {
      countMines(board, i, j).toString
    }
    else {
      BOMB.toString
    }
  }

  def calculate(row: Int, column: Int, board: String): String = {
    this.columns = column
    this.rows = row
    val builder = new StringBuilder()

    for (i <- 0 until row) {
      builder.append(DELIMITER)
      for (j <- 0 until column) {
        val index = j + i * column
        builder.append(getField(board, index, i, j))
      }
    }
    builder.toString()
  }

}
