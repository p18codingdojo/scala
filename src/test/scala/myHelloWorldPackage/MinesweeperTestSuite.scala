package myHelloWorldPackage

import org.scalatest.FunSuite

class MinesweeperTestSuite extends FunSuite {

  test("The 1x1 field without bomb should return 0") {
    var result : String = Minesweeper.calculate(1,1,".")
    assert(result == "\n0")
  }

  test("The 1x1 field with bomb should return *") {
    var result : String = Minesweeper.calculate(1,1,"*")
    assert(result == "\n*")
  }

  test("The 2x1 board without mines should return 00") {
    var result : String = Minesweeper.calculate(2,1,"..")
    assert(result == "\n0\n0")
  }

  test("The 1x2 board without mines should return 00") {
    var result : String = Minesweeper.calculate(1,2,"..")
    assert(result == "\n00")
  }

  test("The 1x2 board with mine in the second place should return 1*") {
    var result : String = Minesweeper.calculate(1,2,".*")
    assert(result == "\n1*")
  }

  test("The 1x3 board with mine in the first place should return *10") {
    var result : String = Minesweeper.calculate(1,3,"*..")
    assert(result == "\n*10")
  }

  test("The 3x2 board with mine in the first place should return *11100") {
    var result : String = Minesweeper.calculate(3,2,"*.....")
    assert(result == "\n*1\n11\n00")
  }

  test("The 4x4 board with mine in the first place should return ") {
    var result : String = Minesweeper.calculate(4,4,"..*.....*.*.....")
    assert(result == "\n01*1\n1322\n*2*1\n1211")
  }

  test("The 4x4 board with mine in the first place should return 2") {
    var result : String = Minesweeper.calculate(4,4,"...............*")
    assert(result == "\n0000\n0000\n0011\n001*")
  }
}
